<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class BlogCategory extends Model
{
    use Sluggable;
    protected $table = 'blog_category';
    protected $fillable = [
        'topic', 'parent_id', 'body', 'image', 'thumbnail' , 'status'
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'topic'
            ]
        ];
    }

    public function count_posts($id)
    {
        $countsBlog = Blog::where('category' , '=' , $id)->get();
        return $countsBlog->count();
    }
}
