<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = 'blog';
    use Sluggable;
    protected $fillable = [
      'topic' , 'category' , 'body' , 'status' , 'author' , 'countView' , 'tags' , 'image',
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'topic'
            ]
        ];
    }

    public function category_name($id)
    {
        $thisCategory = BlogCategory::findOrFail($id);
        return $thisCategory->topic;
    }

    public function author_name($id)
    {
        $thisAuthor = User::findOrFail($id);
        return $thisAuthor->name;
    }

    public function path()
    {
        return "blog/$this->id";
    }

}
