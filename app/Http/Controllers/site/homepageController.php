<?php

namespace App\Http\Controllers\site;


use App\Comment;
use App\Product;
use App\ProductCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class homepageController extends Controller
{
    public function index()
    {
        if (cache('allProducts')){
            $allProducts = cache('allProducts');
        } else{
            $allProducts = Product::latest()->take(8)->get();
            cache(['allProducts' => $allProducts] , Carbon::now()->addMinute(5));
        }
        return view('site.homePage.homePage', compact('allProducts'));
    }

    public function comment(Request $request, Comment $comment)
    {
        $this->validate($request, [
           'comment_body' => 'required|min:5'
        ]);

        Comment::create([
            'user_id' => auth()->user()->id ,
            'comment_body' => $request->get('comment_body'),
            'parent_id' => $request->get('parent_id'),
            'commentable_id' => $request->get('commentable_id'),
            'commentable_type' => $request->get('commentable_type')
        ]);
        return back();
    }
}
