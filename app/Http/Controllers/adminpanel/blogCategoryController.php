<?php

namespace App\Http\Controllers\adminpanel;

use App\BlogCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class blogCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allBlogCategories2 = BlogCategory::latest()->paginate(10);
        return view('adminpanel.blogCategory.index', compact('allBlogCategories2'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $allBlogCategories = BlogCategory::all();
        return view('adminpanel.blogCategory.create' , compact('allBlogCategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = '';
        if ($request->hasFile('image')){
            $destination = public_path() . config('cms-setting.url_blog_category');
            if (!is_dir($destination)) {
                mkdir($destination, 0777, true);
            }
            $destination = $destination . '/';
            $file = $request->file('image');
            $filename = time() . $file->getClientOriginalName();
            $file->move($destination, $filename);
            $image = config('cms-setting.url_blog_category') . '/' . $filename;
        }
        else{
            $image = null;
        }

        $description = $request->get('description') ?? null;

        BlogCategory::create([
            'topic' => $request->get('topic'),
            'body'   => $description,
            'image' => $image,
            'thumbnail' => $image,
            'status' => $request->get('status'),
            'parent_id' => $request->get('parent_id'),
        ]);

        return redirect()->route('dashboard.blogCategory.index')->with('message', 'دسته بندی با موفقیت در سایت ثبت شد.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $blogCategory = BlogCategory::findOrFail($id);
            $allBlogCategories = BlogCategory::all();
            return view('adminpanel.blogCategory.edit' , compact(['blogCategory' , 'allBlogCategories']));
        }catch (\Exception $e){
            return redirect()->route('dashboard.blogCategory.index')->with('warning' , 'همچین دسته بندی در سایت موجود نیست.');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $blogCategory = BlogCategory::findOrFail($id);
            if ($blogCategory) {
                $this->validate($request,[
                    'topic' => 'required',
                    'parent_id' => 'required',
                    'status' => 'required',
                ],[
                    'topic.required' => 'عنوان دسته بندی الزامی می باشد.',
                    'parent_id.required' => 'انتخاب دسته بندی الزامی می باشد.',
                    'status.required' => 'وضعیت دسته بندی الزامی می باشد.',
                ]);
                $destination = '';
                if ($request->hasFile('image')){
                    $destination = public_path() . config('cms-setting.url_blog_category');
                    if (!is_dir($destination)) {
                        mkdir($destination, 0777, true);
                    }
                    $destination = $destination . '/';
                    $file = $request->file('image');
                    $filename = time() . $file->getClientOriginalName();
                    $file->move($destination, $filename);
                    $image = config('cms-setting.url_blog_category') . '/' . $filename;
                    $thumbnail = $image;
                }
                else{
                    $image = $request->get('image');
                    $thumbnail = $image;
                }

                $blogCategory->update([
                    'topic' => $request->get('topic'),
                    'parent_id' => $request->get('parent_id'),
                    'status' => $request->get('status'),
                    'image' => $image,
                    'thumbnail' => $image,
                    'description' => $request->get('description') ?? null,
                ]);

                $blogCategory->save();
                return redirect()->route('dashboard.blogCategory.index')->with('message' , 'دسته بندی با موفقیت ویرایش شد.');
            }
            else{
                return redirect()->route('dashboard.blogCategory.index')->with('error' , 'همچین دسته بندی در سایت موجود نمی باشد.');
            }
        }
        catch (\Exception $e) {
            return redirect()->route('dashboard.blogCategory.index')->with('error' , 'متاسفانه خطایی در سیستم رخ داده است. لطفا با پشتیبانی تماس بگیرید.');
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $blogCategory = BlogCategory::findOrFail($id);
            if (!$blogCategory) {
                return redirect()->route('dashboard.blog.index')->with('error' ,'همچین دسته بندی در سایت وجود ندار.');
            }
            else {
                $blogCategory->delete();
                return redirect()->route('dashboard.blogCategory.index')->with('warning' , 'دسته بندی بلاگ از سایت پاک شد.');
            }
        }
        catch (\Exception $e) {
            return redirect()->route('dashboard.blogCategory.index')->with('error' , 'متاسفانه خطایی در سیستم رخ داده است. لطفا با پشتیبانی تماس بگیرید.');
        }
    }
}
