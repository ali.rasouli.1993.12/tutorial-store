<?php

namespace App\Http\Controllers\adminpanel;

use App\Order;
use App\Payment;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ordersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Payment::where('status', 'OK')->with('order', 'user')->paginate(10);
        return view('adminpanel.order.index' , compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Payment::with('order', 'user', 'address')->findOrFail($id);
        $allproductsIds = explode(',' , $order->order->products_id);
        $allproductsIds = str_replace(['{' , '}'] , '' , $allproductsIds);
        $all = [];
        foreach ($allproductsIds as $product){
            if (Product::find($product)){
                array_push($all, Product::find($product));
            }
            else{
                return redirect()->route('dashboard.orders.index')->with('error' , 'مشکل در لیست محصولات وجود دارد');
            }
        }

        $selectedProductsNamesAsList = [];
        $selectedProductsPricesAsList = [];
        $selectedProductsIdsAsList = [];
        $selectedProductsCountsAsList = [];
        $selectedProductsTotalPriceAsList = [];
        $TotalPrice = 0; // with Tax
        $TotalPriceWithNoTax = 0; // Without Tax

        foreach (explode(',' , str_replace(['{' , '}'] , '' , $order->order->products_count)) as $single) {
            array_push($selectedProductsCountsAsList , $single);
        }

        foreach (explode(',' , str_replace(['{' , '}'] , '' , $order->order->products_id)) as $single) {
            array_push($selectedProductsIdsAsList , $single);
        }

        foreach (explode(',' , str_replace(['{' , '}'] , '' , $order->order->products_name)) as $single) {
            array_push($selectedProductsNamesAsList , $single);
        }

        foreach ($all as $single){
            array_push($selectedProductsPricesAsList, $single->price);
            $TotalPrice += $single->price;
        }

        for ($i = 0; $i < count($selectedProductsPricesAsList); $i++){
            $currentPrice = $selectedProductsPricesAsList[$i] * $selectedProductsCountsAsList[$i];
            array_push($selectedProductsTotalPriceAsList,
                ($currentPrice) );
            $TotalPriceWithNoTax += $currentPrice;
        }

        return view('adminpanel.order.show' ,
            compact([
                'order',
                'selectedProductsNamesAsList',
                'selectedProductsPricesAsList',
                'selectedProductsIdsAsList',
                'selectedProductsCountsAsList',
                'selectedProductsTotalPriceAsList',
                'TotalPrice',
                'TotalPriceWithNoTax'
            ]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Payment::with('order', 'user', 'address')->findOrFail($id);
        return $order;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
