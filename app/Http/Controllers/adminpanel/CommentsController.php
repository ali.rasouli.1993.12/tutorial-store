<?php

namespace App\Http\Controllers\adminpanel;

use App\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comments = Comment::with('user')->where('confirmed' , 1)->latest()->paginate(10);
        return view('adminpanel.comments.index', compact('comments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Comment::findOrFail($id);
        $comment->delete();
        return redirect()->route('dashboard.comments.index')->with('message', 'کامنت مورد نظر شما از سایت پاک شد.');
    }

    public function unConfirmed()
    {
        $comments = Comment::with('user')->where('confirmed' , 0)->latest()->paginate(10);
        return view('adminpanel.comments.unconfirmed', compact('comments'));
    }

    public function ChangeConfirmed($id)
    {
        $comment = Comment::findOrFail($id);
        $comment->update([
            'confirmed' => 1
        ]);
        $comment->save();
        return redirect()->route('dashboard.comments.unconfirmedComment')->with('message', 'کامنت مورد نظر شما تایید شد.');
    }
}
