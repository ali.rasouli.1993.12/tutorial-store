<?php

namespace App\Http\Controllers\adminpanel;

use App\Blog;
use App\BlogCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class blogController extends Controller
{
    protected $allBlog, $allCategory;

    public function __construct()
    {
        $this->allBlog = Blog::latest()->paginate(10);
//        $this->allBlog = Blog::all();
        $this->allCategory = BlogCategory::all();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = $this->allBlog;
        return view('adminpanel.blog.index' , compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $allCategories = $this->allCategory;
        return view('adminpanel.blog.create' , compact('allCategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'topic' => 'required',
            'tags' => 'required',
            'category' => 'required',
            'status' => 'required',
            'image' => 'required',
            'description' => 'required',
        ] , [
            'topic.required' => 'فیلد عنوان الزامی است.',
            'tags.required' => 'فیلد تگ ها الزامی است.',
            'category.required' => 'فیلد دسته بندی الزامی است.',
            'status.required' => 'فیلد وضعیت الزامی است.',
            'image.required' => 'فیلد تصویر الزامی است.',
            'description.required' => 'فیلد متن توضیحات الزامی است.',
        ]);
        
        try {


            if ($request->hasFile('image')) {
                $image = '';

                $destination = public_path('').config('cms-setting.url_blog');
                if (!is_dir($destination)) {
                    mkdir($destination, 0777, true);
                }
                $destination = $destination . '/';
                $file = $request->file('image');
                $filename = time() . $file->getClientOriginalName();
                $file->move($destination, $filename);
                $image = config('cms-setting.url_blog') . '/' . $filename;
//
//                dd([
//                    'topic' => $request->get('topic'),
//                    'tags' => $request->get('tags')?? '',
//                    'category' => $request->get('category'),
//                    'status' => $request->get('status'),
//                    'image' => $image,
//                    'body' => $request->get('description')?? '',
//                    'author' => Auth::user()->id,
//                    'countView' => 0
//                ]);
                Blog::create([
                    'topic' => $request->get('topic'),
                    'tags' => $request->get('tags')?? null,
                    'category' => $request->get('category'),
                    'status' => $request->get('status'),
                    'image' => $image?? null,
                    'body' => $request->get('description')?? null,
                    'author' => Auth::user()->id,
                    'countView' => 0,
                ]);
                return redirect()->route('dashboard.blog.index')->with('message' , 'پست جدید با موفقیت در سایت ثبت شد.');
            }
        } catch (\Exception $e) {
            return redirect()->route('dashboard.blog.index')->with('error' , $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function uploadImage()
    {
        $this->validate(request(),[
            'upload' => 'required'
        ]);
        $image = '';

        $imagePath = "/upload/images/2020/";
        $file = request()->file('upload');
        $filename = $file->getClientOriginalName();
        if(file_exists(public_path($imagePath) . $filename)) {
            $filename = Carbon::now()->timestamp . $filename;
        }

        $file->move(public_path($imagePath) , $filename);
        $url = $imagePath . $filename;

        return "<script>window.parent.CKEDITOR.tools.callFunction(1 , '{$url}' , '')</script>";
    }
}
