<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'order';
    protected $fillable = [
        'user_id', 'products_id', 'products_name', 'products_count', 'amount', 'description', 'delivery_address', 'postal_code'
        , 'company_name', 'discount'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id' , 'id');
    }
}
