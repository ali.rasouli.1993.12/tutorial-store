<?php

namespace App\Providers;

use App\Blog;
use App\BlogCategory;
use App\ProductCategory;
use Illuminate\Support\ServiceProvider;

class ProductCategoryCustomServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('site.layout.header.header-bottom' , function($view){
            $allCategories = ProductCategory::all();
            return $view->with('allCategories' , $allCategories);
        });

        view()->composer('site.shop.index' , function($view){
            $allCategories = ProductCategory::all();
            return $view->with('allCategories' , $allCategories);
        });

        view()->composer('site.blog.sidebar', function ($view) {
            $allBlogCategories = BlogCategory::all();
            return $view->with('allBlogCategories', $allBlogCategories);
        });

        view()->composer('site.blog.sidebar', function ($view) {
            $Bests = Blog::where('countView' , '>' , 0)->orderBy('countView' , 'desc')->limit(4)->get();
            return $view->with('Bests' ,  $Bests );
        });

        view()->composer('site.blog.sidebar', function ($view) {
            $Tags = Blog::where('tags' , '!=' , null)->orderBy('id' , 'desc')->limit(30)->get();
            return $view->with('Tags' ,  $Tags );
        });

        view()->composer('site.productCategory.productCategorySidebar', function ($view) {
            $allCategories = ProductCategory::all();
            return $view->with('allCategories' , $allCategories);
        });

    }
}
