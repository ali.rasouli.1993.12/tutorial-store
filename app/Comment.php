<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
      'user_id', 'comment_body', 'confirmed', 'commentable_id', 'commentable_type', 'parent_id'
    ];

    public function commentable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class , 'user_id' , 'id');
    }

    public function setCommentBodyAttribute($value)
    {
        $this->attributes['comment_body'] = str_replace(PHP_EOL, '<br>' , $value);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class, 'parent_id' , 'id')->where('confirmed' , 1);
    }

    public function deleteNoNeedComments($id)
    {
        $comment = Comment::findOrFail($id);
        if ($comment->parent_id) {
            return 'true';
        }
        else {
            return 'null';
        }
    }
}
