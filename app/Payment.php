<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payment';
    protected $fillable = [
        'order_id', 'amount', 'RefID', 'card_pan', 'status'
    ];

    public function order()
    {
        return $this->belongsTo(Order::class,'order_id' , 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id' , 'id');
    }

    public function address()
    {
        return $this->belongsTo(Address::class, 'user_address_id', 'id');
    }

    public function products()
    {
        return $this->hasManyThrough(Product::class,Order::class,'products_id','id');
    }

//
//    public function address()
//    {
//        return $this->hasManyThrough(Address::class, User::Class, 'id' , 'user_id' );
//    }
}
