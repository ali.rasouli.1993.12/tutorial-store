@extends('adminpanel.layout')

@section('pageTitle')
    همه ی نوشته ها
@stop

@section('mainContent')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">همه نوشته ها</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">داشبورد</a></li>
                        <li class="breadcrumb-item active">نوشته ها</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    @if(session('message'))
                        <div class="alert alert-success col-sm-12">
                            <li>{{ Session::get('message') }}</li>
                        </div>
                    @endif

                    @if(session('error'))
                        <div class="alert alert-success col-sm-12">
                            <li>{{ Session::get('error') }}</li>
                        </div>
                    @endif

                    @if(session('warning'))
                        <div class="alert alert-warning col-sm-12">
                            <li>{{ Session::get('warning') }}</li>
                        </div>
                    @endif
                </div>
                <div class="col-12">
                    <div class="card">

                        <div class="card-header">
                            <h3 class="card-title text-right">جدول نوشته ها</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            @if(\App\Blog::all()->count() > 0)
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>ردیف</th>
                                        <th>عنوان</th>
                                        <th>اسلاگ</th>
                                        <th>توضیح کوتاه عنوان</th>
                                        <th>تعداد بازدید</th>
                                        <th>دسته بندی</th>
                                        <th>تاریخ انتشار</th>
                                        <th>وضعیت انتشار</th>
                                        <th>عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($blogs as $blog)
                                        <tr>
                                            <td>{{ $blog->id }}</td>
                                            <td>{{ $blog->topic }}</td>
                                            <td>{{ $blog->slug }}</td>
                                            <td>{!! str_limit($blog->body, 120) !!}</td>
{{--                                            <td class="text-center">--}}
{{--                                                <img width="100" src="{{ url('') }}{{ $blog->image }}" alt="{{ $blog->title }}">--}}
{{--                                            </td>--}}
                                            <td>{{ $blog->category_name($blog->category) }}</td>
                                            <td>{{ $blog->countView }}</td>
{{--                                            <td>{{ date_format($blog->created_at , 'Y/M/D') }}</td>--}}
                                            <td>{{ Verta::instance($blog->created_at)->format('Y-n-j H:i:s') }}</td>
                                            <td>
                                                @if($blog->status == 0)
                                                    پیش نویس
                                                @else
                                                    منتشر شده
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('dashboard.product.edit' , $blog->id) }}" style="color: gray"><i class="fas fa-pencil-alt"></i></a>
                                                <a href="{{ route('dashboard.product.destroy' , $blog->id) }}" style="color: red"><i class="fas fa-trash-alt"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>

                            @else
                                <h4>نوشته ای در سایت موجود نیست</h4>
{{--                                <a class="btn btn-outline-success" href="{{ route('dashboard.product.create') }}">اولین محصول خود را بساز</a>--}}
                            @endif
                        </div>

                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@stop

@section('footerScripts')

    <script !src="">
        $('.nav-link').removeClass('active');

        $('#blogs').addClass('menu-open');
        $('#blogs > a').addClass('active');
        $('#allBlogs').addClass('active');

    </script>


@stop
