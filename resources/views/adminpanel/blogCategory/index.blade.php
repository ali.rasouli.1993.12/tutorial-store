@extends('adminpanel.layout')

@section('pageTitle')
    همه ی دسته بندی ها
@stop

@section('mainContent')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark d-inline-block">همه دسته بندی ها</h1>
                    <span class="mr-2"><a class="btn btn-outline-success" href="{{ route('dashboard.blogCategory.create') }}">افزودن دسته بندی جدید</a></span>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">داشبورد</a></li>
                        <li class="breadcrumb-item active">دسته بندی ها</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    @if(session('message'))
                        <div class="alert alert-success col-sm-12">
                            <li>{{ Session::get('message') }}</li>
                        </div>
                    @endif

                    @if(session('error'))
                        <div class="alert alert-danger col-sm-12">
                            <li>{{ Session::get('error') }}</li>
                        </div>
                    @endif

                    @if(session('warning'))
                        <div class="alert alert-warning col-sm-12">
                            <li>{{ Session::get('warning') }}</li>
                        </div>
                    @endif
                </div>
                <div class="col-12">
                    <div class="card">

                        <div class="card-header">
                            <h3 class="card-title text-right">جدول دسته بندی ها</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            @if(\App\BlogCategory::all()->count() > 0)
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>ردیف</th>
                                        <th>نام دسته بندی</th>
                                        <th>اسلاگ</th>
                                        <th>تصویر</th>
                                        <th>توضیح</th>
                                        <th>وضعیت</th>
                                        <th>عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($allBlogCategories2 as $BlogCategory)
                                        <tr>
                                            <td>{{ $BlogCategory->id }}</td>
                                            <td>{{ $BlogCategory->topic }}</td>
                                            <td>{{ $BlogCategory->slug }}</td>
                                            <td class="text-center">
                                                <img width="100" src="{{ url('') }}{{ $BlogCategory->image }}" alt="{{ $BlogCategory->topic }}">
                                            </td>
                                            <td>{!! $BlogCategory->body !!}</td>
                                            <td>
                                                @if($BlogCategory->status == 0)
                                                    پیش نویس
                                                @else
                                                    منتشر شده
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('dashboard.blogCategory.edit' , $BlogCategory->id) }}" style="color: gray"><i class="fas fa-pencil-alt"></i></a>
                                                <a href="{{ route('dashboard.blogCategory.destroy' , $BlogCategory->id) }}" style="color: red"><i class="fas fa-trash-alt"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>

                            @else
                                <h4>دسته بندی ای در سایت موجود نیست</h4>
                                {{--                                <a class="btn btn-outline-success" href="{{ route('dashboard.product.create') }}">اولین محصول خود را بساز</a>--}}
                            @endif
                        </div>

                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@stop

@section('footerScripts')

    <script !src="">
        $('.nav-link').removeClass('active');

        $('#blogs').addClass('menu-open');
        $('#blogs > a').addClass('active');
        $('#newBlogCategory').addClass('active');

    </script>


@stop
