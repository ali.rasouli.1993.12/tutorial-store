@extends('adminpanel.layout')

@section('pageTitle')
    داشبورد Dashboard
@stop

 @section('mainContent')
     <div class="content-header">
         <div class="container-fluid">
             <div class="row mb-2">
                 <div class="col-sm-6">
                     <h1 class="m-0 text-dark">داشبورد</h1>
                 </div><!-- /.col -->
                 <div class="col-sm-6">
                     <ol class="breadcrumb float-sm-left">
                         <li class="breadcrumb-item active">داشبورد</li>
                     </ol>
                 </div><!-- /.col -->
             </div><!-- /.row -->
         </div><!-- /.container-fluid -->
     </div>
     <section class="content">
         <div class="container-fluid">
             <div class="row">
                 <div class="col-lg-3 col-6">
                     <!-- small box -->
                     <div class="small-box bg-info">
                         <div class="inner">
                             <h3>{{ $allMembers }}</h3>

                             <p>تعداد کاربران</p>
                         </div>
                         <div class="icon">
                             <i class="ion ion-person-add"></i>
                         </div>
                         <a href="#" class="small-box-footer"><i class="fas fa-arrow-circle-right"></i> همه کاربران</a>
                     </div>
                 </div>
             </div>
         </div>
     </section>
 @stop

@section('footerScripts')
    <script !src="">
        $('.nav-link').removeClass('active');
        $('#dashboard').addClass('active');
        $('#dashboard > a').addClass('active');
    </script>
@stop
