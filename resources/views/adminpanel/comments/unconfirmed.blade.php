@extends('adminpanel.layout')

@section('pageTitle')
    نظرات تایید نشده
@stop

@section('mainContent')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">همه نظرات تایید نشده</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-left">
                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">داشبورد</a></li>
                    <li class="breadcrumb-item active">همه نظرات تایید نشده</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->


<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @if(session('message'))
                <div class="alert alert-success col-sm-12">
                    <li>{{ Session::get('message') }}</li>
                </div>
                @endif

                @if(session('error'))
                <div class="alert alert-success col-sm-12">
                    <li>{{ Session::get('error') }}</li>
                </div>
                @endif
            </div>
            <div class="col-12">
                <div class="card">

                    <div class="card-header">
                        <h3 class="card-title text-right">جدول نظرات تایید نشده</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        @if(\App\Comment::where('confirmed' , 0)->count() > 0)
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>ردیف</th>
                                <th>عنوان نظر</th>
                                <th>نام کاربر</th>
                                <th>نام نوشته (محصول)</th>
                                <th>عملیات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($comments as $comment)
                            <tr>
                                <td></td>
                                <td>{!! $comment->comment_body !!}</td>
                                <td>{{ $comment->user->name }}</td>
                                <td><a target="_blank" href="{{ $comment->commentable->path() }}">{{ $comment->commentable->title }}</a></td>
                                <td>
                                    <a href="{{ route('dashboard.comments.changeConfirmed' , $comment->id) }}" style="color: green">
                                        <i class="fas fa-check"></i>
                                    </a>
                                    <a href="{{ route('dashboard.comments.destroy' , $comment->id) }}" style="color: red">
                                        <i class="fas fa-trash-alt"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>

                        </table>
                        {!! $comments->render() !!}
                        @else
                            <h4>هیچ کامنت تایید نشده ای در سایت موجود نیست.</h4>
                        @endif
                    </div>

                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
@stop
