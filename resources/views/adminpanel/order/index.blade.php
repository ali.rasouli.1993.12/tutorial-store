@extends('adminpanel.layout')

@section('pageTitle')
    سفارشات
@stop

@section('mainContent')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">همه سفارشات</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">داشبورد</a></li>
                        <li class="breadcrumb-item active">همه سفارشات</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    @if(session('message'))
                        <div class="alert alert-success col-sm-12">
                            <li>{{ Session::get('message') }}</li>
                        </div>
                    @endif

                    @if(session('error'))
                        <div class="alert alert-success col-sm-12">
                            <li>{{ Session::get('error') }}</li>
                        </div>
                    @endif
                </div>
                <div class="col-12">
                    <div class="card">

                        <div class="card-header">
                            <h3 class="card-title text-right">جدول سفارشات</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            @if(\App\Payment::where('status' , 'OK')->count() > 0)
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>ردیف</th>
                                        <th>شماره سفارش</th>
                                        <th>مبلغ سفارش</th>
                                        <th>نام سفارش دهنده</th>
                                        <th>وضعیت پرداخت</th>
                                        <th>تاریخ ثبت سفارش</th>
                                        <th>عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($orders as $order)
                                        <tr>
                                            <td></td>
                                            <td>{{ $order->order->id }}</td>
                                            <td>{{ number_format($order->amount) }} تومان</td>
                                            <td>{{ $order->user->name }}</td>
                                            <td>پرداخت موفق</td>
                                            <td>{{ Verta::instance($order->order->created_at) }}</td>
                                            <td style="font-size: 18px">
                                                <a href="{{ route('dashboard.orders.show' , $order->id) }}" style="color: green">
                                                    <i class="fas fa-check"></i>
                                                </a>
                                                <a href="{{ route('dashboard.comments.destroy' , $order->id) }}" style="color: red">
                                                    <i class="fas fa-trash-alt"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                                {!! $orders->render() !!}
                            @else
                                <h4>سفارشی در سایت ثیت نشده است.</h4>
                            @endif
                        </div>

                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@stop
