@extends('adminpanel.layout')

@section('pageTitle')
    شماره سفارش {{ $order->id }}
@stop

@section('mainContent')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">نام سفارش دهنده: {{ $order->user->name }} </h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">داشبورد</a></li>
                        <li class="breadcrumb-item active">سفارش</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">

                        <div class="card-header">
                            <h3 class="card-title text-right">سفارش شماره {{ $order->id }}</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <ul class="row show-order">

                                <li class="col-md-3">
                                    نام سفارش دهنده:
                                    <span>{{ $order->user->name }}</span>
                                </li>

                                <li class="col-md-3">
                                    شماره پیگیری:
                                    <span>{{ $order->RefID }}</span>
                                </li>

                                <li class="col-md-3">
                                    وضعیت پرداخت:
                                    <span>موفق</span>
                                </li>

                                <li class="col-md-3">
                                    شماره همراه:
                                    <span>{{ $order->user->mobile?? '' }}</span>
                                </li>

                                <li class="col-md-4">
                                    تاریخ ثبت سفارش:
                                    <span>{{ Verta::instance($order->created_at) }}</span>
                                </li>

                                <li class="col-md-7">
                                    توضیحات اضافی:
                                    <span>{{ $order->order->description ? $order->order->description : 'بدون توضیح' }}</span>
                                </li>

                                <li class="col-md-3">
                                    میزان مالیات پرداختی:
                                    <span>{{ number_format( $order->order->amount - $TotalPriceWithNoTax ) }} تومان</span>
                                </li>

                                <li class="col-md-12">
                                    <hr>
                                    <p>سفارش:</p>
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <th class="th-row">ردیف</th>
                                            <th>نام کالا</th>
                                            <th>تعداد</th>
                                            <th>قیمت واحد</th>
                                            <th>قیمت کل</th>
                                        </thead>
                                        <tbody>
                                            @for($i = 0 ;$i < count($selectedProductsIdsAsList); $i++)
                                                <tr>
                                                    <td class="th-row">{{ $i + 1 }}</td>
                                                    <td>
                                                        <a href="{{ route('site.product.index' , $selectedProductsIdsAsList[$i]) }}"
                                                        target="_blank">
                                                            {{ $selectedProductsNamesAsList[$i] }}
                                                        </a>
                                                    </td>
                                                    <td class="th-row2">{{ $selectedProductsCountsAsList[$i] }}</td>
                                                    <td class="th-row2">{{ number_format($selectedProductsPricesAsList[$i]) }} تومان</td>
                                                    <td class="th-row2">{{ number_format($selectedProductsTotalPriceAsList[$i]) }} تومان</td>
                                                </tr>
                                            @endfor
                                        <tr>
                                            <td colspan="4" class="text-left">مجموع سبد خرید (بهمراه نرخ مالیات 9 درصد) :</td>
                                            <td colspan="1" class="text-center">{{ number_format($order->order->amount) }} تومان</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <hr>
                                </li>

                                <li class="col-sm-12">
                                    آدرس:
                                    <span>{{ $order->address->user_address }}</span>
                                </li>
                            </ul>
                            <br>
{{--                            <pre>--}}
{{--                                {{ $order }}--}}
{{--                            </pre>--}}
                        </div>

                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@stop
