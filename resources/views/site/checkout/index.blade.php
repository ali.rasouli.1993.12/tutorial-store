@extends('site.layout.inc.main')
@section('page-title')
    جزئیات صورت حساب
@stop

@section('main-content')
    <div class="page-header text-center" style="background-image: url('site/assets/images/page-header-bg.jpg')">
        <div class="container">
            <h1 class="page-title">صورت حساب<span>فروشگاه</span></h1>
        </div><!-- End .container -->
    </div>
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">خانه</a></li>
                <li class="breadcrumb-item"><a href="{{ route('site.shop.index') }}">فروشگاه</a></li>
                <li class="breadcrumb-item active" aria-current="page">جزئیات صورت حساب</li>
            </ol>
        </div><!-- End .container -->
    </nav>
    <div class="page-content">
        <div class="cart">
            <div class="container">
                <div class="row">
                    @if(Cart::content()->count() == 0)
                        <div class="col-sm-12">
                            <div class="alert alert-danger col-sm-9 text-right">
                                <li style="list-style: none">لطفا به صفحه فروشگاه رفته و خرید خود را آغاز کنید.</li>
                            </div>
                        </div>
                    @else
                        @if(session('success_message'))
                            <div class="alert alert-success col-sm-9 text-right">
                                <li style="list-style: none">{{ Session::get('success_message') }}</li>
                            </div>
                        @endif

                        @if(session('error'))
                            <div class="alert alert-danger col-sm-9 text-right">
                                <li style="list-style: none">{{ Session::get('error') }}</li>
                            </div>
                        @endif

                        @if(Auth::guest())
                            <div class="alert alert-warning col-sm-9 text-right">
                                <li style="list-style: none">شما هنوز وارد سایت نشده اید. لطفا قبل از سفارش، لاگین کنید.</li>
                                <a class="btn btn-primary" href="{{ route('login') }}">ورود به سایت</a>
                            </div>
                        @else
                            <form action="#" method="POST" style="width: 100%">
                                    <div class="row">
                                        @csrf
                                        <div class="col-lg-9">
                                            <h2 class="checkout-title">جزئیات صورت حساب</h2><!-- End .checkout-title -->
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <label>نام و نام خانوادگی *</label>
                                                    <input type="text" name="name" class="form-control" required="" value="{{ auth()->user()->name ?? '' }}">
                                                </div><!-- End .col-sm-6 -->
                                            </div><!-- End .row -->

                                            <div class="row address-section">
                                                <input type="hidden" value="0" name="address_choose_id" id="address_choose_id">
                                                <labek class="col-sm-12 text-right">انتخاب آدرس : *</labek>
                                                @if($current_addresses != null)
                                                    @foreach($current_addresses as $address)
                                                        <div class="col-sm-12 col-md-6">
                                                            <div class="form-control" style="height: 100%; text-align: right">
                                                                <input class="address_choose" type="radio" name="address_id" id="{{ $address->id }}" value="{{ $address->id }}">
                                                                <label for="{{ $address->id }}">{{ $address->user_address }}</label>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endif
                                                <div class="col-sm-12">
                                                    <div class="form-control" style="height: auto">
                                                        <input class="address_choose" type="radio" name="address_id" id="0" value="0">
                                                        <label for="0">ارسال به آدرس جدید</label>
                                                        <br>
                                                        <br>
                                                        <label>نام شرکت یا موسسه (اختیاری)</label>
                                                        <input type="text" class="form-control" name="company_name">

                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <label>شهر / استان *</label>
                                                                <input type="text" name="city" class="form-control">

                                                            </div><!-- End .col-sm-6 -->

                                                            <div class="col-sm-6">
                                                                <label>شهرستان *</label>
                                                                <input type="text" name="state" class="form-control">
                                                            </div><!-- End .col-sm-6 -->
                                                        </div><!-- End .row -->
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <label>آدرس کامل *</label>
                                                                <input type="text" class="form-control" name="address" placeholder="نام خیابان و پلاک">

                                                            </div><!-- End .col-sm-12 -->

                                                        </div><!-- End .row -->

                                                    </div>
                                                </div>

                                            </div>



                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label>کد پستی *</label>
                                                    <input type="text" maxlength="10" class="form-control" name="postal_code" required="required">
                                                </div><!-- End .col-sm-6 -->

                                                <div class="col-sm-6">
                                                    <label>تلفن ثابت یا موبایل *</label>
                                                    <input type="tel" maxlength="11" class="form-control" name="phone" required="required" value="{{ auth()->user()->mobile ?? '' }}">
                                                </div><!-- End .col-sm-6 -->
                                            </div><!-- End .row -->

                                            <label>ایمیل (اختیاری)</label>
                                            <input type="email" class="form-control" value="{{ auth()->user()->email ?? '' }}">

{{--                                            <div class="custom-control custom-checkbox">--}}
{{--                                                <input type="checkbox" class="custom-control-input" id="checkout-diff-address">--}}
{{--                                                <label class="custom-control-label" for="checkout-diff-address">ارسال به یک آدرس متفاوت؟</label>--}}
{{--                                                <br><br>--}}
{{--                                                <label>آدرس متفاوت :</label>--}}
{{--                                                <textarea class="form-control" cols="30" rows="4" placeholder="آدرس گیرنده جدید را وارد نمایید."></textarea>--}}
{{--                                            </div><!-- End .custom-checkbox -->--}}

                                            <label>توضیحات (اختیاری)</label>
                                            <textarea class="form-control" cols="30" rows="4" placeholder="شما میتوانید توضیحات اضافی خود را اینجا بنویسید"></textarea>
                                        </div><!-- End .col-lg-9 -->
                                        <aside class="col-lg-3">
                                            <div class="summary">
                                                <h3 class="summary-title">سفارش شما</h3><!-- End .summary-title -->

                                                <table class="table table-summary">
                                                    <thead>
                                                    <tr>
                                                        <th>محصول</th>
                                                        <th class="text-left">جمع کل</th>
                                                    </tr>
                                                    </thead>

                                                    <tbody>
                                                    @foreach(Cart::content() as $item)
                                                        <tr>
                                                            <td><a href="#">{{ $item->model->title }}</a></td>
                                                            <td class="text-left">{{ number_format($item->model->price * $item->qty) }} تومان</td>
                                                        </tr>
                                                    @endforeach
                                                    <tr class="summary-subtotal">
                                                        <td>جمع سبد خرید</td>
                                                        <td class="text-left">{{ Cart::subtotal() }} تومان</td>
                                                    </tr>
                                                    <tr>
                                                        <td>شیوه ارسال : </td>
                                                        <td class="text-left">پست پیشتاز</td>
                                                    </tr>
                                                    <tr>
                                                        <td>مبلغ ارسال : </td>
                                                        <td class="text-left">ثابت {{ number_format(10000) }} تومان</td>
                                                    </tr>

                                                    <tr>
                                                        <td>مالیات بر ارزش افزوده : </td>
                                                        <td class="text-left"> {{ Cart::tax() }} تومان</td>
                                                    </tr>

                                                    <tr class="summary-total">
                                                        <td>مبلغ قابل پرداخت :</td>
                                                        <td class="text-left">{{ number_format(str_replace(',','',Cart::total()) + 10000) }} تومان</td>
                                                    </tr>
                                                    <!-- End .summary-total -->
                                                    </tbody>
                                                </table><!-- End .table table-summary -->

                                                <div class="accordion-summary" id="accordion-payment">
                                                    <div class="card">
                                                        <div class="card-header" id="heading-1">
                                                            <h2 class="card-title">
                                                                <a role="button" data-toggle="collapse" href="#collapse-1" aria-expanded="true" aria-controls="collapse-1" class="">
                                                                    درگاه زرین پال
                                                                </a>
                                                            </h2>
                                                        </div><!-- End .card-header -->
                                                        <div id="collapse-1" class="collapse show" aria-labelledby="heading-1" data-parent="#accordion-payment" style="">
                                                            <div class="card-body">
                                                                استفاده از درگاه واسط زرین پال
                                                            </div><!-- End .collapse -->
                                                        </div><!-- End .card -->
                                                    </div><!-- End .accordion -->

                                                    <button type="submit" class="btn btn-outline-primary-2 btn-order btn-block">
                                                        <span class="btn-text">ثبت</span>
                                                        <span class="btn-hover-text">پرداخت</span>
                                                    </button>
                                                </div>
                                            </div><!-- End .summary -->
                                        </aside><!-- End .col-lg-3 -->
                                    </div>
                            </form>
                        @endif


                    @endif
                </div><!-- End .row -->
            </div><!-- End .container -->
        </div><!-- End .cart -->
    </div>
@stop

@section('footer_scripts')
    <script !src="">
        $(document).ready(function () {
            $('.address_choose').click(function () {
                let hiddenInput = $(this).parent().parent().parent().find('#address_choose_id');
                hiddenInput.val($(this).val());
                //console.log(hiddenInput);
                //console.log(hiddenInput.val($(this).val()));
            });
        });
    </script>
@endsection
