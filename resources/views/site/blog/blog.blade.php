@extends('site.layout.inc.main')
@section('page-title')
 بلاگ
@stop

@section('main-content')
    <div class="page-header text-center" style="background-image: url('site/assets/images/page-header-bg.jpg')">
        <div class="container">
            <h1 class="page-title">همه پست ها<span>بلاگ</span></h1>
        </div><!-- End .container -->
    </div><!-- End .page-header -->

    <nav aria-label="breadcrumb" class="breadcrumb-nav mb-2">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">خانه</a></li>
                <li class="breadcrumb-item active" aria-current="page">بلاگ</li>
            </ol>
        </div><!-- End .container -->
    </nav><!-- End .breadcrumb-nav -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    @foreach($allPosts as $post)
                        <article class="entry entry-list">
                        <div class="row align-items-center">
                            <div class="col-md-5">
                                <figure class="entry-media">
                                    <a href="#">
                                        <img src="{{ url('') }}{{ $post->image }}" alt="{{ $post->topic }}">
                                    </a>
                                </figure><!-- End .entry-media -->
                            </div><!-- End .col-md-5 -->

                            <div class="col-md-7">
                                <div class="entry-body">
                                    <div class="entry-meta">
                                        <span class="entry-author">
                                            نویسنده : <a href="#">{{ $post->author_name($post->author) }}</a>
                                        </span>
                                        <span class="meta-separator">|</span>
                                        <a href="#">22 فروردین 1399</a>
                                        <span class="meta-separator">|</span>
                                        <a href="#">2 دیدگاه</a>
                                    </div><!-- End .entry-meta -->

                                    <h2 class="entry-title">
                                        <a href="#">{{ $post->topic }}</a>
                                    </h2><!-- End .entry-title -->

                                    <div class="entry-cats">
                                        <a href="{{ route('site.blog.category.index' , $post->id) }}">{{ $post->category_name($post->category) }}</a>
                                    </div><!-- End .entry-cats -->

                                    <div class="entry-content">
                                       {!! str_limit($post->body , 120) !!}
                                        <a href="#" class="read-more" style="width:20%">ادامه
                                            خواندن</a>
                                    </div><!-- End .entry-content -->
                                </div><!-- End .entry-body -->
                            </div><!-- End .col-md-7 -->
                        </div><!-- End .row -->
                    </article>
                    @endforeach

                    {!! $allPosts->links() !!}
                </div><!-- End .col-lg-9 -->
                @include('site.blog.sidebar')
            </div><!-- End .row -->
        </div><!-- End .container -->
    </div>
@endsection

