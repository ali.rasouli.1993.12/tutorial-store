@if(auth()->check())
    <!-- Reply section -->
    <div class="reply">
        <div class="heading">
            <h3 class="title">ارسال یک دیدگاه</h3><!-- End .title -->
            <p class="title-desc">ایمیل شما منتشر نخواهد شد، فیلد های اجباری با علامت * مشخص
                شده اند.</p>
        </div><!-- End .heading -->

        <form action="{{ route('site.comment') }}" method="post">
            <label for="reply-message" class="sr-only">دیدگاه</label>
            @csrf
            <input type="hidden" name="parent_id" value="0">
            <input type="hidden" name="commentable_id" value="{{ $subject->id }}">
            <input type="hidden" name="commentable_type" value="{{ get_class($subject) }}">
            <textarea name="comment_body" id="reply-message" cols="30" rows="4" class="form-control" required=""
                      placeholder="دیدگاه شما *"></textarea>

            <button type="submit" class="btn btn-outline-primary-2 float-right">
                <span>ارسال دیدگاه</span>
                <i class="icon-long-arrow-left"></i>
            </button>
        </form>
    </div><!-- End Reply section -->

@else
    <div class="alert alert-danger col-sm-12" style="margin-bottom: 20px">
        برای ثبت نظر لطفا ابتدا لاگین کنید.
    </div>
@endif
<h3>({{ count($comments) }}) نظر</h3>
<ul>
    @foreach($comments as $comment)
        <li class="parent_comment">
            <div class="comment">
                <figure class="comment-media">
                    <a href="#">
                        <img src="{{ url('site/assets/images/users/1.jpg') }}" alt="User name">
                    </a>
                </figure>

                <div class="comment-body">
                    {{--                    <a href="#" class="comment-reply">پاسخ</a>--}}
                    <button type="button" class="comment-reply" data-toggle="modal" data-target="#SendReplyCommentModal"
                            data-parent_id="{{ $comment->id }}">پاسخ
                    </button>
                    <div class="comment-user">
                        <h4><a href="#">{{ $comment->user->name }}</a></h4>
                        <span class="comment-date">{{ $comment->updated_at }}</span>
                    </div> <!-- End .comment-user -->

                    <div class="comment-content">
                        <p>
                            {!! $comment->comment_body !!}
                        </p>
                    </div><!-- End .comment-content -->
                </div><!-- End .comment-body -->
            </div><!-- End .comment -->
            <ul class="javab">
                @foreach($comment->comments as $child)
                    <li>
                    <div class="comment">
                        <figure class="comment-media">
                            <a href="#">
                                <img src="{{ url('site/assets/images/users/2.jpg') }}" alt="User name">
                            </a>
                        </figure>

                        <div class="comment-body">
{{--                            <button type="button" class="comment-reply" data-toggle="modal" data-target="#SendReplyCommentModal"--}}
{{--                                    data-parent_id="{{ $child->id }}">پاسخ--}}
{{--                            </button>--}}
                            <div class="comment-user">
                                <h4><a href="#">{{ $child->user->name }}</a></h4>
                                <span class="comment-date">{{ $child->created_at }}</span>
                            </div><!-- End .comment-user -->

                            <div class="comment-content">
                                <p>{!! $child->comment_body !!}</p>
                            </div><!-- End .comment-content -->
                        </div><!-- End .comment-body -->
                    </div><!-- End .comment -->
                </li>
                @endforeach
            </ul>
        </li>
    @endforeach
</ul>

<div class="modal fade" id="SendReplyCommentModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">پاسخ بخ نظر</h5>
            </div>
            <form method="post" action="{{ route('site.comment') }}">
                <div class="modal-body">
                    <div class="col-sm-12">
                            @csrf
                            <input type="hidden" name="parent_id" value="0">
                            <input type="hidden" name="commentable_id" value="{{ $subject->id }}">
                            <input type="hidden" name="commentable_type" value="{{ get_class($subject) }}">
                            <div class="form-group">
                                <label for="message-text" class="col-form-label">متن پیام:</label>
                                <textarea class="form-control" name="comment_body" id="comment_body"></textarea>
                            </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">انصراف</button>
                    <button type="submit" class="btn btn-primary">ارسال پاسخ</button>
                </div>
            </form>
        </div>
    </div>
</div>
