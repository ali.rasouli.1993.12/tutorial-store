<div class="header-top">
    <div class="container">
        <div class="header-left">
            <a href="tel:#"><i class="icon-phone"></i>تلفن تماس : 02155667788</a>
        </div>
        <!-- End .header-left -->

        <div class="header-right">

            <ul class="top-menu">
                <li>
                    <a href="#">لینک ها</a>
                    <ul>
                        <li>
                            <div class="header-dropdown">
                                <a href="#">فارسی</a>
                                <div class="header-menu">
                                    <ul>
                                        <li><a href="#">انگلیسی</a></li>
                                        <li><a href="#">فرانسوی</a></li>
                                        <li><a href="#">ترکی استانبولی</a></li>
                                    </ul>
                                </div>
                                <!-- End .header-menu -->
                            </div>
                        </li>
                        @auth()
                            <li><a href="{{ route('user.account.index') }}">{{ auth()->user()->name }} خوش آمدید.</a></li>
                            <li><a href="{{ route('logoutUser') }}">خروج</a></li>
                            @else
                                <li><a href="{{ route('login') }}" >ورود / ثبت نام</a></li>
                            @endauth

                    </ul>
                </li>
            </ul>
            <!-- End .top-menu -->
        </div>
        <!-- End .header-right -->

    </div>
    <!-- End .container -->
</div>
<!-- End .header-top -->
