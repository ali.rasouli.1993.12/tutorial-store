<?php

return [
    'url' => '/product_image',
    'url_product_category' => '/product_category_image',
    'url_blog_category' => '/blog_category_image',
    'url_blog' => '/blog_category',
];
