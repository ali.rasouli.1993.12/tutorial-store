<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/',  'site\homepageController@index')->name('home');

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

//Route::get('/homePage' , 'site\homepageController@index')->name('home');

Route::get('/login/sendcode' , 'Auth\LoginController@sendCode')->name('login_send_code');

Route::group(['middleware' => ['auth' , 'admin'] ],function() {
    Route::get('/dashboard' , 'adminpanel\dashboardController@index')->name('dashboard');

    Route::group(['prefix' => '/dashboard' , 'namespace' => 'adminpanel'] , function (){

        // Product
        Route::group(['prefix' => '/products' ] , function () {
            Route::get('/' , 'productController@index')->name('dashboard.product.index');
            Route::get('/create' , 'productController@create')->name('dashboard.product.create');
            Route::post('/store' , 'productController@store')->name('dashboard.product.store');
            Route::get('/edit/{id}' , 'productController@edit')->name('dashboard.product.edit');
            Route::post('/update/{id}' , 'productController@update')->name('dashboard.product.update');
            Route::get('/destroy/{id}' , 'productController@destroy')->name('dashboard.product.destroy');
        });

        // ProductCategory
        Route::group(['prefix' => '/productCategory'] , function () {
            Route::get('/' , 'productCategoryController@index')->name('dashboard.productCategory.index');
            Route::get('/create' , 'productCategoryController@create')->name('dashboard.productCategory.create');
            Route::post('/store' , 'productCategoryController@store')->name('dashboard.productCategory.store');
            Route::get('/edit/{id}' , 'productCategoryController@edit')->name('dashboard.productCategory.edit');
            Route::post('/update/{id}' , 'productCategoryController@update')->name('dashboard.productCategory.update');
            Route::get('/destroy/{id}' , 'productCategoryController@destroy')->name('dashboard.productCategory.destroy');
        });

        // ProductAttr
        Route::group(['prefix' => '/productAttr'] , function () {
            Route::get('/' , 'ProductAttribiuesController@index')->name('dashboard.productAttr.index');
            Route::get('/create' , 'ProductAttribiuesController@create')->name('dashboard.productAttr.create');
            Route::post('/store' , 'ProductAttribiuesController@store')->name('dashboard.productAttr.store');
            Route::get('/edit/{id}' , 'ProductAttribiuesController@edit')->name('dashboard.productAttr.edit');
            Route::post('/update/{id}' , 'ProductAttribiuesController@update')->name('dashboard.productAttr.update');
            Route::get('/destroy/{id}' , 'ProductAttribiuesController@destroy')->name('dashboard.productAttr.destroy');
        });
        Route::post('/save_image','productController@uploadImage');

        // Members
        Route::group(['prefix' => '/members'] , function () {
            Route::get('/' , 'MembersController@index')->name('dashboard.members.index');
            Route::get('/create' , 'MembersController@create')->name('dashboard.members.create');
            Route::post('/store' , 'MembersController@store')->name('dashboard.members.store');
            Route::get('/edit/{id}' , 'MembersController@edit')->name('dashboard.members.edit');
            Route::post('/update/{id}' , 'MembersController@update')->name('dashboard.members.update');
            Route::get('/destroy/{id}' , 'MembersController@destroy')->name('dashboard.members.destroy');
        });

        // Blog
        Route::group(['prefix' => '/blog'] , function () {
            Route::get('/' , 'blogController@index')->name('dashboard.blog.index');
            Route::get('/create' , 'blogController@create')->name('dashboard.blog.create');
            Route::post('/store' , 'blogController@store')->name('dashboard.blog.store');
            Route::get('/edit/{id}' , 'blogController@edit')->name('dashboard.blog.edit');
            Route::post('/update/{id}' , 'blogController@update')->name('dashboard.blog.update');
            Route::get('/destroy/{id}' , 'blogController@destroy')->name('dashboard.blog.destroy');
        });

        // BlogCategory
        Route::group(['prefix' => '/blogCategory'] , function () {
            Route::get('/' , 'blogCategoryController@index')->name('dashboard.blogCategory.index');
            Route::get('/create' , 'blogCategoryController@create')->name('dashboard.blogCategory.create');
            Route::post('/store' , 'blogCategoryController@store')->name('dashboard.blogCategory.store');
            Route::get('/edit/{id}' , 'blogCategoryController@edit')->name('dashboard.blogCategory.edit');
            Route::post('/update/{id}' , 'blogCategoryController@update')->name('dashboard.blogCategory.update');
            Route::get('/destroy/{id}' , 'blogCategoryController@destroy')->name('dashboard.blogCategory.destroy');
        });

        Route::group(['prefix' => '/comment'] , function () {
            Route::get('/' , 'CommentsController@index')->name('dashboard.comments.index');
//            Route::get('/create' , 'blogCategoryController@create')->name('dashboard.blogCategory.create');
//            Route::post('/store' , 'blogCategoryController@store')->name('dashboard.blogCategory.store');
//            Route::get('/edit/{id}' , 'blogCategoryController@edit')->name('dashboard.blogCategory.edit');
//            Route::post('/update/{id}' , 'blogCategoryController@update')->name('dashboard.blogCategory.update');
            Route::get('/destroy/{id}' , 'CommentsController@destroy')->name('dashboard.comments.destroy');
        });

        Route::get('/unconfirmedComment', 'CommentsController@unConfirmed')->name('dashboard.comments.unconfirmedComment');
        Route::get('/unconfirmedComment/{id}', 'CommentsController@ChangeConfirmed')->name('dashboard.comments.changeConfirmed');

        Route::group(['prefix' => '/order'] , function () {
            Route::get('/' , 'ordersController@index')->name('dashboard.orders.index');
//            Route::get('/create' , 'blogCategoryController@create')->name('dashboard.blogCategory.create');
//            Route::post('/store' , 'blogCategoryController@store')->name('dashboard.blogCategory.store');
//            Route::get('/edit/{id}' , 'ordersController@edit')->name('dashboard.orders.edit');
            Route::get('/show/{id}' , 'ordersController@show')->name('dashboard.orders.show');
//            Route::post('/update/{id}' , 'blogCategoryController@update')->name('dashboard.blogCategory.update');
//            Route::get('/destroy/{id}' , 'CommentsController@destroy')->name('dashboard.comments.destroy');
        });

    });

});

Route::group(['prefix' => '/account' , 'namespace' => 'userpanel'] , function (){
    Route::get('/' , 'dashboardController@index')->name('user.account.index');
});

Route::group(['namespace' => 'site'] , function (){
    //product
    Route::get('/product/{id}', 'ProductController@show')->name('site.product.index');
    Route::get('/productCategory/{id}', 'ProductCategoryController@show')->name('site.productCategory.index');

    //cart
    Route::get('/cart', 'CartController@index')->name('site.cart.index');
    Route::post('/cart', 'CartController@store')->name('site.cart.store');
    Route::post('/update/i', 'CartController@update')->name('site.cart.update.i');
    Route::post('/update/d', 'CartController@update2')->name('site.cart.update.d');
    Route::delete('/cart/{product}', 'CartController@destroy')->name('site.cart.destroy');


    Route::get('/cart/empty' , 'CartController@empty');

    //checkout
//    Route::get('/checkout', 'CheckoutController@index')->name('site.checkout.index');

    Route::group(['middleware' => 'auth:web'] , function () {
        Route::get('/checkout' , 'CheckoutController@index')->name('site.checkout.index');
        Route::post('/checkout' , 'CheckoutController@store')->name('site.checkout.store');
    });

    Route::get('/checkout/verify' , 'CheckoutController@verifyCheck');

    //shop
    Route::get('/shop' , 'ShopController@index')->name('site.shop.index');

    //blog
    Route::get('/blog' , 'BlogController@index')->name('site.blog.index');
    Route::get('/blog/{id}' , 'BlogController@show')->name('site.blog.category.index');

    //search
    Route::get('/search' , 'ProductController@search')->name('site.product.search');

    // Comment
    Route::post('/comment' , 'homepageController@comment')->name('site.comment');
});
//logout User
Route::get('/logoutUser' , 'Auth\LoginController@logout')->name('logoutUser');
